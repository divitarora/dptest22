package com.maf.test.dptest19;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Dptest22Application {

	public static void main(String[] args) {
		SpringApplication.run(Dptest22Application.class, args);
	}

}
